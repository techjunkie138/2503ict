<?php
    $posts = array();
      $posts[0] = array(
        'image' => 'images/bday.jpg',
        'date' => '06 April 2012',
        'message' => 'Happy birthday!1',
        'number' => rand(1, 10));
      $posts[1] = array(
        'image' => 'images/bday.jpg',
        'date' => '07 April 2012',
        'message' => 'Happy birthday!2',
        'number' => rand(1, 10));
      $posts[2] = array(
        'image' => 'images/bday.jpg',
        'date' => '08 April 2012',
        'message' => 'Happy birthday!3',
        'number' => rand(1, 10));
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
   
  </head>
  
  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Photos</a></li>
              <li><a href="../navbar-static-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
      <!-- Main body -->
      <div class='row'>
          <div class='col-sm-4'>
              <form>
                  Name: <br>
                  <input type="text" name="name"> <br>
                  Message: <br>
                  <textarea rows='5' cols='22'>Enter message here</textarea> <br>
                  <button>Enter</button>
              </form>
          </div>
          
          <div class='col-sm-8'>
            <?php foreach ($posts as $post) { ?>
              <div class="post">
                <div class="number">
                  <p> <?= $post['number'] ?> </p>
                </div>
                <div class="post_image">
                  <img src=" <?= $post['image'] ?> ">
                </div>
                <div class="post_date">
                  <p> <?= $post['date'] ?> </p>
                </div>
                <div class="post_message">
                  <p> <?= $post['message'] ?> </p>
                </div>
              </div>
            <?php } // End of loop ?>
          </div>

      </div>  <!-- /row -->
    </div>  <!-- /container -->
    
  </body>
</html>