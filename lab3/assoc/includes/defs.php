<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($input) {
    global $pms; 

    // Filter $pms by $input
    if (!empty($input)) {
	$results = array();
	foreach ($pms as $pm) {
	    if ((stripos($pm['name'], $input) !== FALSE or 
	    	strpos($pm['to'], $input) !== FALSE or
	    	strpos($pm['from'], $input) !== FALSE or
	    	stripos($pm['party'], $input) !== FALSE or
	    	stripos($pm['state'], $input) !== FALSE)) {
		$results[] = $pm;
	    }
	}
	$pms = $results;
    }

    return $pms;
}
?>
